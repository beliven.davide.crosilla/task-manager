{{-- <input
    {{ $disabled ? 'disabled' : '' }}
    {!! $attributes->merge(['class' => 'outline-none rounded-xl transition border-gray-300 focus:border-cyan-400 focus:ring focus:ring-cyan-200 focus:ring-opacity-50']) !!}
> --}}

@props([
    'name',
    'type' => 'text',
    'disabled' => false,
    'css' => 'w-full outline-none rounded-xl transition border-gray-300 focus:border-cyan-400 focus:ring focus:ring-cyan-200 focus:ring-opacity-50',
    'arg' => null
])

<div
    {!! $attributes -> merge(['class' => 'w-full mt-3']) !!}
>
    <x-label name="{{ $name }}">{{ $name }}</x-label>

    @if ($type === 'select')
        <select name="{{ $name }}" id="{{ $name }}" wire:model.defer="{{ $name }}" class="{{ $css }}" {{ $arg ?? '' }}>
            {{ $slot }}
        </select>
    @elseif ($type === 'textarea')
        <textarea name="{{ $name }}" id="{{ $name }}" wire:model.defer="{{ $name }}" class="{{ $css }}" rows="4" {{ $disabled ?? '' }}></textarea>
    @else
        <input
            type="{{ $type }}"
            name="{{ $name }}"
            id="{{ $name }}"
            value="{{ old($name) }}"
            wire:model.defer="{{ $name }}"
            class="{{ $css }}"
            {{ $disabled ?? '' }}
        >
    @endif
</div>

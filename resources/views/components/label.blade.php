@props(['name'])

<label for="{{ $name }}" {{ $attributes->merge(['class' => 'w-full block font-medium text-md text-gray-700 flex flex-row items-baseline justify-between']) }}>
    <span>{{ ucwords($slot) }}</span>
    @error($name)
        <span class="text-red-500 text-sm">{{ $message }}</span>
    @enderror
</label>

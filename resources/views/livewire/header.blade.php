<div class="bg-custom-dark-gray rounded-xl px-8">
    <div class="flex items-center justify-between h-16">
        <div class="flex items-center">
            <div class="flex-shrink-0">
                <x-application-logo />
            </div>
            <div class="hidden md:block">
                <div class="ml-8 flex items-baseline space-x-4">

                    <a href="{{ route('projects') }}" class="{{ Request::is('*projects*') ? 'active-section' : 'inactive-section' }}">Projects</a>

                    <a href="{{ route('team') }}" class="{{ Request::is('*team*') ? 'active-section' : 'inactive-section' }}">Team</a>

                    <a href="{{ route('clients') }}" class="{{ Request::is('*clients*') ? 'active-section' : 'inactive-section' }}">Clients</a>

                </div>
            </div>
        </div>

        <div class=" md:block">
            <div class="ml-4 flex items-center md:ml-6">

                <!-- Profile dropdown -->
                <div class="ml-3 relative" x-data="{ show: false }" @click.away="show = false">
                    <div class="flex flex-row items-center font-semibold">
                        <span @click="show = !show" class="text-white mr-4 text-sm cursor-pointer">Welcome back, {{ auth() -> user() -> name }}!</span>
                        <button @click="show = !show" type="button"
                            class="max-w-xs bg-gray-800 rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-custom-dark-gray focus:ring-cyan-400 transition"
                            id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                            <x-avatar :src="auth() -> user() -> getAvatar()" />
                        </button>
                    </div>

                    <div
                        x-cloak
                        x-show="show"
                        x-transition:enter="transition ease-out duration-200"
                        x-transition:enter-start="transform opacity-0 scale-95"
                        x-transition:enter-end="transform opacity-100 scale-100"
                        x-transition:leave="transition ease-in duration-75"
                        x-transition:leave-start="transform opacity-100 scale-100"
                        x-transition:leave-end="transform opacity-0 scale-95"
                        class="z-50 origin-top-right absolute right-0 mt-2 w-48 rounded-xl shadow-lg py-2 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                        role="menu" aria-orientation="vertical"
                        aria-labelledby="user-menu-button"
                        tabindex="-1">

                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();"
                                class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 transition" role="menuitem" tabindex="-1" id="user-menu-item-2">
                                Sign out
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

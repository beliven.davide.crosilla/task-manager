<div class="mt-0.5 relative" x-data="{ show: false }">

    <button @click="show = !show" type="button" class="relative rounded-full text-left cursor-pointer focus:outline-none focus:ring-2 focus:ring-cyan-200 sm:text-sm transition">
        <x-status :item="$item" :priority="$item -> priority ?? null" :css="$css" />
    </button>

    @if (auth() -> user() -> can('view', $item))
        <div
            x-cloak
            x-show="show"
            x-transition:enter="transition ease-out duration-200"
            x-transition:enter-start="transform opacity-0"
            x-transition:enter-end="transform opacity-100"
            x-transition:leave="transition ease-in duration-100"
            x-transition:leave-start="transform opacity-100"
            x-transition:leave-end="transform opacity-0"
            @click.away="show = false"
            class="z-15 absolute mt-1 -ml-3 bg-white border border-gray-100 rounded-xl p-2 space-y-2 text-base sm:text-sm flex flex-col"
        >
            @foreach ($statuses as $status)
                @if ($item -> status != $status)
                    <div class="cursor-pointer" wire:click.prevent="changeStatus({{ $status -> id }})" @click="show = false">
                        <x-status :status="$status" />
                    </div>
                @endif
            @endforeach
        </div>
    @endif

</div>

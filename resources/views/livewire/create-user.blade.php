<div
    class="fixed z-10 inset-0 overflow-y-auto"
    aria-labelledby="modal-title"
    role="dialog"
    aria-modal="true"
    x-show="show"
    x-transition:enter="transition ease-out duration-300"
    x-transition:enter-start="transform opacity-0"
    x-transition:enter-end="transform opacity-100"
    x-transition:leave="transition ease-in duration-200"
    x-transition:leave-start="transform opacity-100"
    x-transition:leave-end="transform opacity-0"
    @keydown.escape.window="show = false"
>

    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

        <div
            class="fixed inset-0 bg-gray-500 bg-opacity-25 transition-opacity"
            aria-hidden="true"
            @click="show = false"
        >
        </div>

        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        <div class="inline-block align-bottom bg-white rounded-xl text-left overflow-hidden transform transition-all sm:my-8 sm:align-middle sm:max-w-5xl sm:w-full">
            <div class="bg-white px-8 py-7">

                <h3 class="text-2xl leading-6 font-semibold text-gray-900 mb-3" id="modal-title">
                    Add a team member
                </h3>

                <div class="absolute right-5 top-4 flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full hover:bg-red-100 transition sm:mx-0 sm:h-10 sm:w-10 cursor-pointer" @click="show = false">
                    <svg class="h-6 w-6 text-red-600" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                    </svg>
                </div>

                <form wire:submit.prevent="createUser" action="#" method="POST">
                    <div class="flex flex-row space-x-4">
                        <x-input :name="'name'" />
                        <x-input :name="'role'" :type="'select'">
                            <option value=""></option>
                            @foreach ($roles as $role)
                                <option value="{{ $role -> id }}">{{ $role -> name }}</option>
                            @endforeach
                        </x-input>
                    </div>

                    <div class="flex flex-row space-x-4">
                        <x-input :name="'email'" :type="'email'" />
                        <x-input :name="'password'" :type="'password'" />
                    </div>

                    <div class="mt-6 sm:flex sm:flex-row-reverse">
                        <x-button>Create</x-button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<div class="inline-block bg-white rounded-xl text-left w-full">
    <div class="bg-white px-6 py-4 rounded-xl">

        <div class="w-full flex flex-row items-start justify-between mb-3">
            <h3 class="text-lg font-semibold text-gray-800">{{ $task -> title }}</h3>

            <livewire:change-status :item="$task" :statuses="$statuses" />
        </div>

        <div class="flex flex-row items-center text-sm w-full leading-4 mt-2 overflow-y-hidden">
            <div class="flex flex-row items-center w-fit-content">
                <x-avatar :size="'h-7 w-7'" :src="$task -> createdBy -> getAvatar()" />
                <span class="fit-content ml-2">{{ $task -> createdBy -> name }}</span>
            </div>
            <svg class="h-6 w-6 mx-3" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M17 8l4 4m0 0l-4 4m4-4H3" />
            </svg>
            <div class="flex flex-row items-center w-max">
                <x-avatar :size="'h-7 w-7'" :src="$task -> assignedTo -> getAvatar()" />
                <span class="fit-content ml-2">{{ $task -> assignedTo -> name }}</span>
            </div>
        </div>

        <div class="mt-3">
            {{ $task -> description }}
        </div>
    </div>
</div>

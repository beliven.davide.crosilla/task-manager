<div
    wire:ignore.self
    x-cloak
    x-show="show"
    x-transition:enter="transition ease-out duration-200"
    x-transition:enter-start="transform opacity-0"
    x-transition:enter-end="transform opacity-100"
    x-transition:leave="transition ease-in duration-100"
    x-transition:leave-start="transform opacity-100"
    x-transition:leave-end="transform opacity-0"
    @click.away="show = false"
    class="z-15 absolute mt-6 right-0 overflow-y-scroll bg-white border border-gray-100 rounded-xl text-base sm:text-sm flex flex-col"
    style="max-height: 600px;"
>
    @foreach ($users as $user)
        <div
            wire:click="userProjectAssignment({{ $user -> id}}, {{ $project -> id }})"
            class="px-4 py-3 whitespace-nowrap flex flex-row items-center justify-between cursor-pointer hover:bg-cyan-200 hover:bg-opacity-20 transition"
            style="min-width: 350px;"
        >
            <div class="flex flex-row items-center space-x-4">
                <x-avatar :src="$user -> getAvatar()" />
                <div class="flex flex-col">
                    <span class="text-sm font-semibold text-gray-900">{{ $user -> name }}</span>
                    <span class="text-sm text-gray-500">{{ $user -> email }}</span>
                </div>
            </div>
            @if ($user -> can('assignedTo', $project))
                <svg class="h-6 w-6 text-cyan-400 ml-8" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                </svg>
            @endif
        </div>
    @endforeach
</div>

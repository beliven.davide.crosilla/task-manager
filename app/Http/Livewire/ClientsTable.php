<?php

namespace App\Http\Livewire;

use App\Models\Client;
use Livewire\Component;
use Livewire\WithPagination;

class ClientsTable extends Component {

    use WithPagination;

    protected $listeners = ['clientCreated'];

    public function render() {
        $clients = Client::withCount('projects') -> paginate(10);

        return view('livewire.clients-table', [
            'clients' => $clients
        ]);
    }

    public function clientCreated() {
        $this -> gotoPage(Client::withCount('projects') -> paginate(10) -> lastPage());
    }

}

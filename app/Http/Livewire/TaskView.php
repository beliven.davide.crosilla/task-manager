<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Task;

class TaskView extends Component {

    public $task;
    public $statuses;

    public function mount(Task $task, $statuses) {
        $this -> task = $task;
        $this -> statuses = $statuses;
    }

    public function render() {
        return view('livewire.task-view');
    }

}

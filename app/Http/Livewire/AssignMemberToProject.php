<?php

namespace App\Http\Livewire;

use App\Models\Project;
use App\Models\User;
use App\Models\UserProject;
use Livewire\Component;

class AssignMemberToProject extends Component {

    public $project;
    public $projectMembers;

    public function mount(Project $project, $projectMembers) {
        $this -> project = $project;
        $this -> projectMembers = $projectMembers;
    }

    public function render() {
        return view('livewire.assign-member-to-project', [
            'users' => User::all()
        ]);
    }

    public function userProjectAssignment($userID, $projectID) {
        if (User::find($userID) -> can('assignedTo', $this -> project)) {
            UserProject::where('user_id', $userID) -> where('project_id', $projectID) -> delete();
        } else {
            $userProject = UserProject::create([
                'user_id' => $userID,
                'project_id' => $projectID
            ]);
        }

        $this -> emit('userAssignment');
    }

}

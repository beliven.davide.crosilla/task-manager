<?php

namespace App\Http\Livewire;

use App\Models\Project;
use Livewire\Component;
use Livewire\WithPagination;

class ProjectShow extends Component {

    use WithPagination;

    public $project;
    public $statuses;
    public $projectMembers;
    public $tasks;

    protected $listeners = ['taskCreated' => 'refreshProject', 'userAssignment' => 'refreshProject'];

    public function mount(Project $project, $statuses, $projectMembers, $tasks) {
        $this -> project = $project;
        $this -> statuses = $statuses;
        $this -> projectMembers = $projectMembers;
        $this -> tasks = $tasks;
    }

    public function render() {
        return view('livewire.project');
    }

    public function refreshProject() {
        $this -> project -> refresh();
    }

}
